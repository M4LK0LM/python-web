from flask import Flask, render_template

def create_app(config):
    app = Flask(__name__)
    app.config.from_object(config)

    @app.route('/index')
    def index():
        return render_template('index.html')

    return app
    
if __name__ == "__main__":
    app.run(host='0.0.0.0')
